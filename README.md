# Web

## Run with docker (Requires [Docker](https://docs.docker.com/get-docker/)):



1. Clone the repo and change directory with `git clone git@bitbucket.org:mihkelmark/web.git && cd web`
2. In the command line, run `docker build . -t artify_web:v1 && docker run -d -p 8080:80 artify_web:v1`
3. Navigate to http://localhost:8080. By default this will consume the api running on your localhost:3009.


If you have Node 10+ and yarn installed, you can run it on your machine. The steps are as follows:

1. `yarn install && yarn serve --port 8079`
2. Navigate to http://localhost:8079

